let map = null;
let location_string = window.location.href;
location_string = location_string.replace("file:///C:/","");
let location_array = location_string.split("/");
window.ubicacion = location_array[location_array.length - 1];
window.ubicacion = window.ubicacion.replace(".html","");
const api_key = "gPjjflrbMv7zJX1xtM8GG5OEzXaTkVS64GfxU8G0";
/**
 * Función para geolocalizar
 *@param desde STRING: variable de fecha YYYY-MM-DD
 *@param hasta STRING: variable de fecha YYYY-MM-DD
 */
search = function(desde = null,hasta = null) {
  let buscar = null;
  if(window.localStorage.busqueda_index === undefined) {
    buscar = [];
    aux = {"desde": desde,"hasta":hasta,"autofecha": new Date(),"estado": null}
    aux.autofecha = aux.autofecha.getTime()
    buscar.push(aux);
    window.localStorage.setItem("busqueda_index", JSON.stringify(buscar));
  } else {
    if(desde === null) {
      buscar = JSON.parse(window.localStorage.busqueda_index);
      if(buscar.length == 0) return false;
      if(!buscar[0]["estado"]) return false;
      desde = buscar[0].desde;
      hasta = buscar[0].hasta;

      $("#input-desde").val(desde);
      $("#input-hasta").val(hasta);
    } else {
      buscar = JSON.parse(window.localStorage.busqueda_index);
      ARR_aux = buscar.filter(function(x) {
        return x["desde"] == desde && x["hasta"] == hasta
      });
      if(ARR_aux.length == 0) {
        aux = {"desde": desde,"hasta":hasta,"autofecha": new Date(),"estado": null};
        aux.autofecha = aux.autofecha.getTime();
        buscar.push(aux);
        window.localStorage.setItem("busqueda_index", JSON.stringify(buscar));
      } else {
        d = new Date();
        ARR_aux[0]["autofecha"] = d.getTime();
      }
    }
  }
  let url = "https://api.nasa.gov/neo/rest/v1/feed?start_date=" + desde + "&end_date=" + hasta + "&api_key=" + api_key;

  ARR_aux = buscar.filter(function(x) {
    return x["desde"] == desde && x["hasta"] == hasta
  });
  $.ajax({
    url: url,
    beforeSend: function() {
      if(window.datos === undefined) window.datos = null;
      $("#r-totales").find("span").text(0);
      $("#buscador").html("<p class=\"text-center text-uppercase\">Buscando información</p>");
    },
    success: function(result){
      window.datos = [];
      $("#buscador").html("");
      console.log(result);
      for(var i in result.near_earth_objects) {
        for(var j in result.near_earth_objects[i]) {
          approach_data = result.near_earth_objects[i][j]["close_approach_data"][0];
          let aux = {};
          aux["id"] = result.near_earth_objects[i][j]["neo_reference_id"];
          aux["nombre"] = result.near_earth_objects[i][j]["name"];
          aux["url"] = result.near_earth_objects[i][j]["nasa_jpl_url"];
          aux["distancia"] = approach_data["miss_distance"]["kilometers"];
          aux["velocidad"] = parseFloat(approach_data["relative_velocity"]["kilometers_per_hour"]).toFixed(2)
          aux["fecha"] = approach_data["epoch_date_close_approach"]
          window.datos.push(aux)
        }
      }
      $("#r-totales").find("span").text(window.datos.length);

      ARR_aux[0]["estado"] = 1;
      window.localStorage.setItem("busqueda_index", JSON.stringify(buscar));
    },
    error: function (errormessage) {
      $("#buscador").html("<p class=\"text-center text-uppercase text-error\">Ocurrió un error</p>");
      ARR_aux[0]["estado"] = 0;
      window.localStorage.setItem("busqueda_index", JSON.stringify(buscar));
    }
  }).done(function(result) {
    mostrarInfo("#buscador");
  });
}
/**
 *
 */
search_img = function(fecha = null,latitud = null, longitud = null) {

  // https://api.nasa.gov/planetary/earth/imagery/?lon=300.75&lat=5.5&date=2014-02-01&cloud_score=True&api_key=DEMO_KEY
  let buscar = null;
  if(window.localStorage.busqueda_imagenes === undefined) {
    buscar = [];
    aux = {"fecha": fecha,"latitud":latitud,"longitud":longitud,"autofecha": new Date(),"estado": null}
    aux.autofecha = aux.autofecha.getTime()
    buscar.push(aux);
    window.localStorage.setItem("busqueda_imagenes", JSON.stringify(buscar));
  } else {
    if(fecha === null) {
      buscar = JSON.parse(window.localStorage.busqueda_imagenes);
      if(buscar.length == 0) return false;
      if(!buscar[0]["estado"]) return false;
      fecha = buscar[0].fecha;
      latitud = buscar[0].latitud;
      longitud = buscar[0].longitud;

      $("#input-date").val(fecha);
      $("#input-latitud").val(latitud);
      $("#input-longitud").val(longitud);
    } else {
      buscar = JSON.parse(window.localStorage.busqueda_imagenes);
      ARR_aux = buscar.filter(function(x) {
        return x["fecha"] == fecha && x["latitud"] == latitud && x["longitud"] == longitud
      });
      if(ARR_aux.length == 0) {
        aux = {"fecha": fecha,"latitud":latitud,"longitud":longitud,"autofecha": new Date(),"estado": null}
        aux.autofecha = aux.autofecha.getTime();
        buscar.push(aux);
        window.localStorage.setItem("busqueda_imagenes", JSON.stringify(buscar));
      } else {
        d = new Date();
        ARR_aux[0]["autofecha"] = d.getTime();
      }
    }
  }
  ARR_aux = buscar.filter(function(x) {
    return x["fecha"] == fecha && x["latitud"] == latitud && x["longitud"] == longitud
  });
  let url = "https://api.nasa.gov/planetary/earth/imagery/?lon=" + longitud + "&lat=" + latitud + "&date=" + fecha + "&cloud_score=True&api_key=" + api_key;
  $.ajax({
    url: url,
    beforeSend: function() {
      $("#buscador").html("<p class=\"text-center text-uppercase\">Buscando información</p>");
    },
    success: function(result){
      html = "";
      fecha = result.date;
      fecha = fecha.replace("T"," ");
      html += "<p class='text-center text-truncate'><strong>ID:</strong> " + result.id + "</p>";
      html += "<p class='text-center'><strong>Fecha:</strong> " + fecha + "</p>";
      html += "<img src='" + result.url + "' class='d-block w-100 mx-auto'/>";
      $("#buscador").html(html)
      ARR_aux[0]["estado"] = 1;
      window.localStorage.setItem("busqueda_imagenes", JSON.stringify(buscar));
    },
    error: function (errormessage) {
      $("#buscador").html("<p class=\"text-center text-uppercase text-error\">Ocurrió un error</p>");
      ARR_aux[0]["estado"] = 0;
      window.localStorage.setItem("busqueda_imagenes", JSON.stringify(buscar));
    }
  }).done(function(result) {

  });
}
/**
 *
 */
eliminarUnHistorial = function(i) {
  if(window.ubicacion == "index") {
    let buscar = JSON.parse(window.localStorage.busqueda_index);
    let x = $(i).closest("p").data("pos");
    delete buscar[x];
    buscar = buscar.filter(Boolean);
    $(i).closest("p").remove();
    window.localStorage.setItem("busqueda_index", JSON.stringify(buscar));
  }
  if(window.ubicacion == "imagenes") {
    let buscar = JSON.parse(window.localStorage.busqueda_imagenes);
    let x = $(i).closest("p").data("pos");
    delete buscar[x];
    buscar = buscar.filter(Boolean);
    $(i).closest("p").remove();
    window.localStorage.setItem("busqueda_imagenes", JSON.stringify(buscar));
  }
  verHistorial();
}
/**
 * Función para relanzar la búsqueda de un elemento
 */
verUnHistorial = function(i) {
  if(window.ubicacion == "index") {
    let buscar = JSON.parse(window.localStorage.busqueda_index);
    let x = $(i).closest("p").data("pos");
    $("#input-desde").val(buscar[x]["desde"]);
    $("#input-hasta").val(buscar[x]["hasta"]);
    search(buscar[x]["desde"],buscar[x]["hasta"]);
  }
  if(window.ubicacion == "imagenes") {
    let buscar = JSON.parse(window.localStorage.busqueda_imagenes);
    let x = $(i).closest("p").data("pos");
    $("#input-date").val(buscar[x]["fecha"]);
    $("#input-latitud").val(buscar[x]["latitud"]);
    $("#input-longitud").val(buscar[x]["longitud"]);
    search_img(buscar[x]["fecha"],buscar[x]["latitud"],buscar[x]["longitud"]);
  }
  $("#modal .close").click();
}
/**
 * Función para ver historial
 */
verHistorial = function() {
  $("body").addClass("modal-open");
  let html = "<p class='text-center text-uppercase'>sin elementos</p>";
  if(!$("#modal").is(":visible")) {
    $("#modal").addClass("show");
  }
  if(window.ubicacion == "index") {
    if(window.localStorage.busqueda_index !== undefined) {
      let buscar = JSON.parse(window.localStorage.busqueda_index);
      if(buscar.length != 0) html = "";
      for(var x in buscar) {
        desde = buscar[x].desde;
        hasta = buscar[x].hasta;
        autofecha = buscar[x].autofecha;
        estado = buscar[x].estado;
        Arr_estado = ["Error en la búsqueda","Activo"];
        html += "<p data-pos='" + x + "' class='position-relative'>" +
                  "<strong class='text-uppercase'>desde</strong> " + desde +
                  "<strong class='text-uppercase'>hasta</strong> " + hasta +
                  (estado ?
                    "<i onclick='eliminarUnHistorial(this)' class=' cursor-pointer ml-1' >[E]</i>" +
                    "<i onclick='verUnHistorial(this)' class='cursor-pointer ml-1' >[V]</i>"
                    :
                    "<i onclick='eliminarUnHistorial(this)' class='cursor-pointer ml-1'>[E]</i>"
                  ) +
                  "<br/><small>" + parseFecha(autofecha) + " | ESTADO: " + Arr_estado[estado] + "</small>" +
                "</p>";
      }
    }
  }
  if(window.ubicacion == "imagenes") {
    if(window.localStorage.busqueda_imagenes !== undefined) {
      let buscar = JSON.parse(window.localStorage.busqueda_imagenes);
      if(buscar.length != 0) html = "";
      for(var x in buscar) {
        fecha = buscar[x].fecha;
        latitud = buscar[x].latitud;
        longitud = buscar[x].longitud;
        autofecha = buscar[x].autofecha;
        estado = buscar[x].estado;
        Arr_estado = ["Error en la búsqueda","Activo"];
        html += "<p data-pos='" + x + "' class='position-relative'>" +
                  "<strong class='text-uppercase'>fecha</strong> " + fecha +
                  "<strong class='text-uppercase'>latitud</strong> " + latitud +
                  "<strong class='text-uppercase'>longitud</strong> " + longitud +
                  (estado ?
                    "<i onclick='eliminarUnHistorial(this)' class=' cursor-pointer ml-1' >[E]</i>" +
                    "<i onclick='verUnHistorial(this)' class='cursor-pointer ml-1' >[V]</i>"
                    :
                    "<i onclick='eliminarUnHistorial(this)' class='cursor-pointer ml-1'>[E]</i>"
                  ) +
                  "<br/><small>" + parseFecha(autofecha) + " | ESTADO: " + Arr_estado[estado] + "</small>" +
                "</p>";
      }
    }
  }
  $("#modal").find(".modal-body").html(html)
}
/**
 * Función para mostrar la información buscada
 *@param target STRING: donde se agrega la información
 *@param limit INT: cantidad a mostrar - por defualt 10; si se ingresa un 0, muestra todo
 *@param desde INT: desde donde va a empezar la iteración
 *@param datos ARRAY: información que se mostrará en la vista
 *-------
 * Los elementos de datos deben respetar la siguiente estructura (porque no valida si esta)
 * datos = [{"id":xx,"nombre":xx,"url":xx,"distancia":xx,"velocidad":xx,"fecha":xx}, (...) ]
 */
mostrarInfo = function(target, limit = 10, desde = 0, datos = window.datos) {
  //if(!$(target).append("<ul>").length) $(target).append("<ul>");
  $(target).html("<ul></ul>")
  //$(target).append("<ul>").html("");
  let length = parseInt(limit);

  if(length > datos.length || length == 0) length = datos.length
  for(var x = 0; x < length; x++) {
    li = "<span onclick='compartir(this)' class='position-absolute border bg-primary text-white' style='right:0; top:0; padding: 5px; cursor: pointer;'>compartir</span>";
    li += "<span class='d-block mb-1'><strong class='mr-1'># ID</strong>" + window.datos[x]["id"] + "</span>";
    li += "<span class='d-block mb-1'><strong class='mr-1'>Nombre</strong>" + window.datos[x]["nombre"] + "</span>";
    li += "<span class='d-block mb-1'><strong class='mr-1'>Fecha</strong>" + parseFecha(window.datos[x]["fecha"]) + "</span>";
    li += "<span class='d-block mb-1'><strong class='mr-1'>URL</strong><a class='text-truncate' href='" + window.datos[x]["url"] + "' target='_blank'>" + window.datos[x]["url"] + "</a></span>"
    li += "<span class='d-block mb-1'><strong class='mr-1'>Distancia</strong>" + window.datos[x]["distancia"] + " km</span>";
    li += "<span class='d-block'><strong class='mr-1'>Velociadad relativa</strong><br/>" + parseFloat(window.datos[x]["velocidad"]).toFixed(2) + " km/s</span>";

    $(target).find("ul").append("<li data-id='" + x + "' class='border p-1 bg-white position-relative'>" + li + "</li>")
  }

  if(datos.length > parseInt(limit) && parseInt(limit)  > 0) {
    $(target).addClass("position-relative");
    $(target).append("<span onclick='mostrarInfo(\"" + target + "\", 0, " + limit + ")' class='mostrar-mas p-1 border bg-light position-absolute' style='bottom:-58px; left:-1px; cursor:pointer'>Cargar más</span>")
  } else {
    if($(target).find(".mostrar-mas")) $(target).find(".mostrar-mas").remove();
  }
}
/**
 *
 */
mostrarUno = function(target,data) {
  e = JSON.parse(data);
  let html = "";
  html += "<span class='d-block mb-1'><strong class='mr-1'># ID</strong>" + e["id"] + "</span>";
  html += "<span class='d-block mb-1'><strong class='mr-1'>Nombre</strong>" + e["nombre"] + "</span>";
  html += "<span class='d-block mb-1'><strong class='mr-1'>Fecha</strong>" + parseFecha(e["fecha"]) + "</span>";
  html += "<span class='d-block mb-1'><strong class='mr-1'>URL</strong><a class='text-truncate' href='" + e["url"] + "' target='_blank'>" + e["url"] + "</a></span>"
  html += "<span class='d-block mb-1'><strong class='mr-1'>Distancia</strong>" + e["distancia"] + " km</span>";
  html += "<span class='d-block'><strong class='mr-1'>Velociadad relativa</strong><br/>" + parseFloat(e["velocidad"]).toFixed(2) + " km/s</span>";

  $(target).html(html);
}
/**
 *@param e INT: fecha pasada a segundos
 *@return retorna el formato DD/MM/YYYY HH:mm
 */
parseFecha = function(e) {
  let date = new Date(e);
  return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + "/" + (date.getMonth() + 1 < 10 ? "0" + date.getMonth() + 1 : date.getMonth() + 1) + "/" + date.getFullYear() +
        " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes());
}
/**
 * Función usada para compartir cierta información. Cada elemento es independiente y no lo realiza en forma masiva.
 *@param t OBJECT DOM: el elemento a compartir contiene este, del padre saca el index del ARRAY de objetos
 */
compartir = function(t) {
  let i = $(t).parent().data("id");
  window.localStorage.setItem("data", JSON.stringify(window.datos[i]));
  window.location = "compartir.html";
}
/**
 * Función que lanza el buscar de fechas
 */
submit = function() {
  let hoy = new Date();
  let hora = hoy.getHours() + ":" + hoy.getMinutes() + ":" + hoy.getSeconds() + ":" + hoy.getMilliseconds();
  if(window.ubicacion == "index") {
    let desde = $("#input-desde").val();
    let hasta = $("#input-hasta").val();
    if(desde == "" || hasta == "") {
      alert("Faltan datos");
      return false;
    }
    let desdeDATE = dates.convert(desde + " " + hora);
    let hastaDATE = dates.convert(hasta + " " + hora);

    if(dates.compare(hoy,desdeDATE) == 1 && dates.compare(hoy,hastaDATE) == 1) {
      if(dates.compare(hastaDATE,desdeDATE) == 1) {
        search(desde,hasta);
      } else alert("La fecha 'desde' no debe superar a 'hasta'");
    } else alert("La fechas no pueden superar o ser el día de hoy");
  }
  if(window.ubicacion == "imagenes") {
    let fecha = $("#input-date").val();
    let latitud = $("#input-latitud").val();
    let longitud = $("#input-longitud").val();
    _return_1 = _return_2 = "";
    $('*[required="true"]').each(function(){
      if($(this).is(":visible")) {
        if($(this).val() == "") {
          if(_return_1 != "") _return_1 += "\n";
          _return_1 += "- " + $(this).data("nombre");
        }
        if($(this).is(":invalid") && $(this).val() != "") {
          if(_return_2 != "") _return_2 += "\n";
          _return_2 += "- " + $(this).data("nombre");
        }
      }
    });
    if(_return_1 != "") {
      alert((_return_1 != "" ? "Faltan datos:\n" + _return_1 + "\n\n" : "") + (_return_2 != "" ? "Datos incorrectos:\n" + _return_2 : ""));
      return false;
    }
    let fechaDATE = dates.convert(fecha + " " + hora);
    if(dates.compare(hoy,fechaDATE) == 1) {
      search_img(fecha,latitud,longitud);
    } else alert("La fecha no puede superar o ser el día de hoy");
  }
}
/**
 * Función que limpia los datos del SUBMIT y resetea el formulario
 */
limpiar = function(l) {
  if($("input").val() != "") {
    if(confirm("¿Seguro de limpiar el registro?")) {
      window.datos = undefined;//limpio variable
      $("input").val("");
      $("#buscador").html("<p class=\"text-center text-uppercase\">Sin búsqueda</p>");
      if(window.ubicacion == "index")
        $("#r-totales").find("span").text(0);
    }
  }
}
/**
 * Función para activar o desactivar la Geolocalización
 *@param t OBJECT DOM
 */
mostrarUbicacion = function(t) {
  if($("#map-canvas").is(":visible")) {
    $(t).removeClass("btn-danger").addClass("btn-warning").text("mostrar");
    $("#map-canvas").removeClass("mt-1").addClass("d-none");
    $("#map-canvas").removeAttr("style");
  } else {
    $(t).removeClass("btn-warning").addClass("btn-danger").text("ocultar");
    //--------
    $("#map-canvas").removeClass("d-none").addClass("mt-1");
    $("#map-canvas").css({height:400});
    init();
  }
}
/**
 * Valida el formulario con elemento required y visible
 *@param t STRING: elemento donde se busca la info
 *@return STRING: cadena de palabras
 */
validar = function(t) {
  let _return_1 = _return_2 = "";
  $(t).find('*[required="true"]').each(function(){
    if($(this).is(":visible")) {
      if($(this).val() == "") {
        if(_return_1 != "") _return_1 += "\n";
        _return_1 += "- " + $(this).data("nombre");
      }
      if($(this).is(":invalid") && $(this).val() != "") {
        if(_return_2 != "") _return_2 += "\n";
        _return_2 += "- " + $(this).data("nombre");
      }
    }
  });
  return (_return_1 == "" ? "" : "FALTAN DATOS\n" + _return_1) +
    (_return_1 != "" && _return_2 != "" ? "\n----------\n" : "") +
    (_return_2 == "" ? "" : "DATOS NO VÁLIDOS\n" + _return_2);
};
/**
 * Función para el envio del formulario
 */
submitFORM = function(t) {
  _v = validar("#" + t.id);
  if(_v == "") {
    $("#" + t.id).find("input,button").attr("disabled",true);
    data = JSON.parse(window.localStorage.getItem("data"))
    email_emisor = t["emisor"].value;
    email_destinatario = t["destinatario"].value;
    nombre = t["nombre"].value;
    body = "";
    body += (nombre != "" ? nombre + " ha compartido la siguiente información." : "Se compartió la siguiente información.");
    body += "\n\n";
    body += "NOMBRE: " + data.nombre;
    body += "\nVELOCIDAD: " + data.velocidad;
    body += "\nDISTANCIA: " + data.distancia;
    body += "\nURL: " + data.url;
    sendMail(email_emisor,email_destinatario,"Información compartida - NASA",body);

    localStorage.removeItem("data");
    window.location = "index.html";
  } else
    alert(_v);
}
/**
 *
 */
sendMail = function(ee,ed,s,b) {
  let link = "mailto:" + ed
             + "?subject=" + encodeURIComponent(s)
             + "&body=" + encodeURIComponent(b);
  let win = window.open(link, '_blank');
  win.focus();
}
/**
 * Función que lanza la Geolocalización
 */
init = function() {
  let option = {
    zoom: 14
  }
  map = new google.maps.Map(document.getElementById("map-canvas"),option);
  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        info = new google.maps.InfoWindow({
          map:map,
          position: pos,
          content: 'Estás aquí'
        });
        map.setCenter(pos);
      },function() {
        handleNoGeolocation(true);
      });
  } else {
    handleNoGeolocation(false);
  }
}
/**
 * Función que muestra error del google.maps
 *@param flag BOOLEANO
 */
handleNoGeolocation = function(flag) {
  if(flag) content = "Error, falló la localización"
  else content = "El navegador no soporta"

  options = {
    map: map,
    position: new google.maps.Latlng(60,105),
    content: content
  }

  InfoWindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position)
}
init = function() {
  let lugar = $("#img-api");
  let url = "https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY";

  $.ajax({
    url: url,
    success: function(result){
      let html = "";
      html += "<img src='" + result["url"] + "'/>";
      html += "<p>" + result["title"] + "</p>";
      lugar.html(html);
    },
  }).done(function(result) {});
}
/**
 *
 */
$(document).ready(function() {
  $(".close").on("click",function() {
    $("body").removeClass("modal-open");
    $("#modal").removeClass("show");
    $("#modal").find(".modal-body").html("");
  });
  init();
  if(window.ubicacion == "contacto") {
    li = "";
    for(var x in window.localStorage) {
      if(["getItem","length","key","setItem","removeItem","clear"].indexOf(x) < 0)
        li += "<li style='width:100%;'><strong>" + x + ":</strong> " + window.localStorage.getItem(x) + "</li>";
    }
    if(li != "") $("#storage").html(li)
  }
  if(window.ubicacion == "index") {
    if(window.datos === undefined) $("#r-totales").find("span").text(0);
    if(window.localStorage.busqueda_index !== undefined) {
      if($("#buscador").length)
        search();
    }
  }
  if(window.ubicacion == "imagenes") {
    if(window.localStorage.busqueda_imagenes !== undefined) {
      if($("#buscador").length)
        search_img();
    }
  }
});
/**
 *@return -1 if a < b
 *@return 0 if a = b
 *@return 1 if a > b
 */
var dates = {
    string: function(d) {
      day = d.getDay();day = (day < 10 ? "0" + day : day);
      month = d.getMonth();month = (month < 10 ? "0" + month : month);
      year = d.getFullYear();
      hour = d.getHours();hour = (hour < 10 ? "0" + hour : hour);
      minute = d.getMinutes();minute = (minute < 10 ? "0" + minute : minute);
      second = d.getSeconds();second = (second < 10 ? "0" + second : second);
      return day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
    },
    convert:function(d) {
        return (
            d.constructor === Date ? d :
            d.constructor === Array ? new Date(d[0],d[1],d[2]) :
            d.constructor === Number ? new Date(d) :
            d.constructor === String ? new Date(d) :
            typeof d === "object" ? new Date(d.year,d.month,d.date) :
            NaN
        );
    },
    compare:function(a,b) {
        return ((a.getTime() === b.getTime()) ? 0 : ((a.getTime() > b.getTime()) ? 1 : - 1));
    }
}
